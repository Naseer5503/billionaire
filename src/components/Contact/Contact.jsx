import "./contact.css";
function Contact() {
    return <>
        <section className="contact mt-32">
            <div className="contact-heading text-white font-garmani font-normal lg:text-[48px] sm:text-[34px] text-[32px] sm:leading-[55px] leading-[42px] wow slideInLeft" data-wow-delay=".5s">
                Buy Billionarie Cat on Pancakeswap
            </div>
            <div className="contact-address wow slideInDown" data-wow-delay=".5s">
                <h3 className="leading-[45px] text-[24px] font-medium">Contract Address:</h3>
                <p className="leading-[45px] text-base text-center sm:text-[22px] mt-[2px] font-normal ">COMING SOON</p>
            </div>
            <div className="contact-buy wow slideInRight" data-wow-delay=".5s">
                <h3 className="text-lg leading-[45px] text-[24px] font-medium">Buy Billionarie Cats</h3>
                <button className="font-medium">BUY</button>
            </div>
        </section>
    </>;
}

export default Contact