import React from 'react'
import "./partners.css";
import partner1 from "../../assets/partners/partner1.png";
import partner2 from "../../assets/partners/partner2.png";
import partner3 from "../../assets/partners/partner3.png";
import partner4 from "../../assets/partners/partner4.png";


function Partners() {
    return <>
        <div className='container mx-auto'>
            <h1 className='partner-heading text-white font-garmani m-auto sm:text-right text-left px-10  text-2xl sm:text-3xl md:text-5xl lg:text-5xl wow fadeInDown' data-wow-delay=".5s"><span className='heading-border'><hr /></span>Listing Partners</h1>
            <section className='partners-img grid lg:grid-cols-4 sm:grid-cols-2 grid-cols-1 justify-between  mb-32 mt-12 m-auto partners flex-wrap  gap-8 wow fadeInUp' data-wow-delay=".5s">
                <PartnersDiv partner={partner1} />
                <PartnersDiv partner={partner2} />
                <PartnersDiv partner={partner3} />
                <PartnersDiv partner={partner4} />
            </section>
        </div>
    </>;
}

const PartnersDiv = (props) => {
    const { value, classes, topLeft = "5px", topRight = "5px", bottomRight = "5px", bottomLeft = "5px", partner } = props;
    return (<img src={partner} className="" alt="" />)

}


export default Partners